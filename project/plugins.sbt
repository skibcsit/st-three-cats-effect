addSbtPlugin("ch.epfl.scala" % "sbt-scalajs-bundler" % "0.21.1")
addSbtPlugin("org.scala-js" % "sbt-scalajs" % "1.13.0")
addSbtPlugin("org.scalablytyped.converter" % "sbt-converter" % "1.0.0-beta41")
addSbtPlugin("com.github.sbt" % "sbt-native-packager" % "1.9.4")

ThisBuild / evictionErrorLevel := Level.Info
