import cats.effect.{IO, IOApp}
import org.scalajs.dom.{Event, MouseEvent, document, window}
import typings.std.global.requestAnimationFrame
import typings.std.FrameRequestCallback
import typings.three.examplesJsmControlsOrbitControlsMod.OrbitControls
import typings.three.examplesJsmLoadersGltfloaderMod.GLTFLoader
import typings.three.mod.{BoxGeometry, BufferGeometry, Camera, DodecahedronGeometry, Mesh, MeshBasicMaterial, PerspectiveCamera, Scene, SphereGeometry, TetrahedronGeometry, WebGLRenderer}
import cats.effect.std.Console
import cats.effect.unsafe.implicits.global

object main extends IOApp.Simple {

  private val seq = for {
    _ <- Console[IO].println("hello")
    renderer <- createRenderer(window.innerWidth.asInstanceOf[Float], window.innerHeight.asInstanceOf[Float])
    scene <-createScene()
    camera <-createPerspectiveCamera(1, 1, 3)

    view <- IO(View(renderer,scene,camera))
    geometry<-createGeometry("sphere")
    //    geometry<-IO(createMagnet("cube"))
    material<-createMeshBasicMaterial("green")
    mesh<-createMesh(geometry,material)

    _<- view.addToScene(mesh)
    _<- view.appendRenderer()
    _ <- IO(view.animate(0))
    _ <- Console[IO].println("before appendButton")
    _ <- IO(view.appendButton())

    //  _<- IO(view.loadModel()) currently doesnt work
  } yield ()

  def run: IO[Unit] = seq

  def createMesh[T](geometry:T,material:MeshBasicMaterial):IO[Mesh[T,MeshBasicMaterial]]=
  {
    val mesh = new Mesh(geometry,material)
    IO(mesh)
  }
  def createScene():IO[Scene] ={
    val scene = new Scene()
    IO(scene)
  }

  def createGeometry(primitive: String): IO[BufferGeometry] = {
    if (primitive=="cube")
    {
      val geom = new BoxGeometry(1, 1, 1, 1, 1, 1)
      IO(geom.asInstanceOf[BufferGeometry])
    }
    else if (primitive =="dodec")
    {
      val geom = new DodecahedronGeometry()
      IO(geom.asInstanceOf[BufferGeometry])
    }
    else if (primitive == "sphere") {
      val geom = new SphereGeometry(1,1,1,1,1,1,1)
      IO(geom.asInstanceOf[BufferGeometry])
    }
    else if (primitive == "tetra") {
      val geom = new TetrahedronGeometry(1)
      IO(geom.asInstanceOf[BufferGeometry])
    }
    else{
      val geom = new BufferGeometry()
      IO(geom)
    }
  }

  def createMeshBasicMaterial(color: String): IO[MeshBasicMaterial] = {
    val material = new MeshBasicMaterial()
    material.color.set(color)
    IO(material)
  }

  def createPerspectiveCamera(x: Float, y: Float, z: Float): IO[PerspectiveCamera] = {
    val camera = new PerspectiveCamera(75, window.innerWidth.asInstanceOf[Float]/window.innerHeight.asInstanceOf[Float])
    camera.position.z = z
    camera.position.y = y
    camera.position.x = x
    camera.lookAt(0, 0, 0)
    IO(camera)
  }

  def createRenderer(sizeX:Float,sizeY:Float):IO[WebGLRenderer]={
    val renderer = new WebGLRenderer()
    renderer.setSize(sizeX, sizeY)
    // renderer.setClearColor(0xffffff, 0) white background
    IO(renderer)
  }
  //This class is a composition of all things we need to see picture in our site
  case class View(renderer: WebGLRenderer,scene: Scene,camera: PerspectiveCamera){

    val controls = new OrbitControls(camera.asInstanceOf[Camera],renderer.domElement)
    val loader = new GLTFLoader()
    def loadModel(): Unit ={
      loader.load("shiba.glb",gltf=>scene.add(gltf.scene))
    }

    def addToScene[T](m: Mesh[T, MeshBasicMaterial]): IO[Unit] = {
      IO(scene.add(m))
    }
    def render():Unit={
      renderer.render(scene, camera)
    }

    def appendRenderer():IO[Unit]={
      IO(document.body.appendChild(renderer.domElement))
    }

    def appendButton():Unit={
      val btn = document.getElementById("cube")
      Console[IO].println(s"appendButton: btn = $btn").unsafeRunAndForget()
      btn.addEventListener[MouseEvent](
        "click",
        addButtonListener(_).unsafeRunAndForget()
      )
    }

    def addButtonListener(e: MouseEvent): IO[Unit] =
      for { //Не исполняется!
        _ <- Console[IO].println("addButtonListener")
        _ <- IO(scene.clear())
        geom <- createGeometry("cube")
        material <- createMeshBasicMaterial("red")
        mesh <- createMesh(geom, material)
        _ <- addToScene(mesh)
      } yield ()

    def appendToHtml():Unit={

      var figbuf = "dodec"
      var colbuf = "green"
      val btn = document.getElementById("cube")
      val btn2 = document.getElementById("dodec")
      val btn3 = document.getElementById("tetra")
      val btn4= document.getElementById("red")
      val btn5 = document.getElementById("green")
      val btn6 =document.getElementById("blue")
      btn.addEventListener("click", { (e: MouseEvent) =>
        scene.clear()
        figbuf = "cube"

        for{//Не исполняется!
          geom<-createGeometry(figbuf)
          material<-createMeshBasicMaterial(colbuf)
          mesh<-createMesh(geom,material)
        }yield(addToScene(mesh))
      })
      //      btn2.addEventListener("click", { (e: MouseEvent) =>
      //        scene.clear()
      //        figbuf = "dodec"
      //        addToScene(new Mesh(createGeometry(figbuf), createMeshBasicMaterial(colbuf)))
      //      })
      //      btn3.addEventListener("click", { (e: MouseEvent) =>
      //        scene.clear()
      //        figbuf = "tetra"
      //        addToScene(new Mesh(createGeometry(figbuf), createMeshBasicMaterial(colbuf)))
      //      })
      //
      //      btn4.addEventListener("click", { (e: MouseEvent) =>
      //        scene.clear()
      //        colbuf = "red"
      //        addToScene(new Mesh(createGeometry(figbuf), createMeshBasicMaterial(colbuf)))
      //      })
      //      btn5.addEventListener("click", { (e: MouseEvent) =>
      //        scene.clear()
      //        colbuf = "green"
      //        addToScene(new Mesh(createGeometry(figbuf), createMeshBasicMaterial(colbuf)))
      //      })
      //      btn6.addEventListener("click", { (e: MouseEvent) =>
      //        scene.clear()
      //        colbuf = "blue"
      //        addToScene(new Mesh(createGeometry(figbuf), createMeshBasicMaterial(colbuf)))
      //      })
    }

    def animate:FrameRequestCallback =
    time=>{
      requestAnimationFrame(animate)
      controls.update()
      render()
      IO(animate)
    }
  }

}
