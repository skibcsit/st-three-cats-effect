var merge = require('webpack-merge');
var generated = require('./scalajs.webpack.config');
var path = require('path');
var webpack = require('webpack');

var local = {
  "ignoreWarnings": [/Failed to parse source map/],
};

module.exports = merge(generated, local);
