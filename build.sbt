ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.10"

lazy val root = (project in file("."))
  .enablePlugins(
    ScalaJSPlugin,
    ScalaJSBundlerPlugin,
    ScalablyTypedConverterPlugin,
    UniversalPlugin,
    UniversalDeployPlugin,
  )
  .settings(
    name := "ScalablyThree",
    Compile / npmDependencies ++= Seq(
      "three" -> "0.150.0",
      "@types/three" -> "0.150.0"
    ),
    Compile / npmDevDependencies ++= Seq(
      "webpack-merge" -> "4.1",
      "@webpack-cli/serve" -> "1.5.2",
    ),
    scalaJSUseMainModuleInitializer := true,
    libraryDependencies ++= Seq(
      "org.scala-js" %%% "scalajs-dom" % "2.2.0",
      "org.typelevel" %%% "cats-effect" % "3.4.8"
    ),
    webpackConfigFile := Some((ThisBuild / baseDirectory).value / "custom.webpack.config.js"),
  )
